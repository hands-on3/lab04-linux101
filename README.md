En este Laboratorio se hará lo siguiente:

1.- Servicio NGINX (vm_lab_01)

Siguiendo este tutorial vas a instalar apache en una maquina virtual generada con vagrant vm_lab_01

https://access.redhat.com/documentation/es-es/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/setting-up-and-configuring-nginx_deploying-different-types-of-servers


2.- Servicio NFS. (vm_lab_02)

siguiendo este tutorial vas a instalar NFS en una maquina virtual generada con vagrant vm_lab_02

https://www.howtoforge.com/nfs-server-and-client-on-centos-7


3.- Configuración de Volúmenes con LVM (vm_lab_03)


https://www.zentica-global.com/es/zentica-blog/ver/como-instalar-y-configurar-lvm-en-centos-7---sugerencia-de-linux-6073a07e63105